From 9f54fcdc2a1f1614c60db3cbd89a4f5b489cd5b1 Mon Sep 17 00:00:00 2001
From: samwhiting <sam@whitings.org>
Date: Fri, 7 Oct 2022 13:20:53 -0400
Subject: [PATCH 11/17] zmq: add option to block or drop samples when high
 watermark is reached

Signed-off-by: Sam <sam@whitings.org>

Signed-off-by: Sam <sam@whitings.org>
Co-authored-by: Sam <shwhiti@sandia.gov>
(cherry picked from commit c49548c2e079fa3b50efbd8d1699b5089d523438)
Signed-off-by: Jeff Long <willcode4@gmail.com>
---
 gr-zeromq/grc/zeromq_pub_sink.block.yml          | 14 ++++++++++++--
 gr-zeromq/include/gnuradio/zeromq/pub_sink.h     |  3 ++-
 gr-zeromq/lib/pub_sink_impl.cc                   | 16 ++++++++++++----
 gr-zeromq/lib/pub_sink_impl.h                    |  3 ++-
 .../python/zeromq/bindings/pub_sink_python.cc    |  3 ++-
 5 files changed, 30 insertions(+), 9 deletions(-)

diff --git a/gr-zeromq/grc/zeromq_pub_sink.block.yml b/gr-zeromq/grc/zeromq_pub_sink.block.yml
index f62eb160a..b683b041c 100644
--- a/gr-zeromq/grc/zeromq_pub_sink.block.yml
+++ b/gr-zeromq/grc/zeromq_pub_sink.block.yml
@@ -39,6 +39,14 @@ parameters:
     label: Filter Key
     dtype: string
     default: ''
+-   id: drop_on_hwm
+    category: Advanced
+    label: On Full Buffer
+    dtype: enum
+    default: 'True'
+    options: ['True', 'False']
+    option_labels: ['Drop', 'Block']
+    hide: 'part'
 
 inputs:
 -   domain: stream
@@ -48,7 +56,7 @@ inputs:
 templates:
     imports: from gnuradio import zeromq
     make: zeromq.pub_sink(${type.itemsize}, ${vlen}, ${address}, ${timeout}, ${pass_tags},
-        ${hwm}, ${key})
+        ${hwm}, ${key}, ${drop_on_hwm})
         
 cpp_templates:
     includes: [ '#include <gnuradio/zeromq/pub_sink.h>' ]
@@ -58,7 +66,9 @@ cpp_templates:
         const_cast<char *>(${address}${'.c_str())' if str(address)[0] not in '"\'' else ')'},
         ${timeout}, 
         ${pass_tags}, 
-        ${hwm});
+        ${hwm},
+        ${key},
+        ${drop_on_hwm});
     link: ['gnuradio::gnuradio-zeromq']      
     translations:
       'True': 'true'
diff --git a/gr-zeromq/include/gnuradio/zeromq/pub_sink.h b/gr-zeromq/include/gnuradio/zeromq/pub_sink.h
index 073c36606..74e0c93a7 100644
--- a/gr-zeromq/include/gnuradio/zeromq/pub_sink.h
+++ b/gr-zeromq/include/gnuradio/zeromq/pub_sink.h
@@ -56,7 +56,8 @@ public:
                      int timeout = 100,
                      bool pass_tags = false,
                      int hwm = -1,
-                     const std::string& key = "");
+                     const std::string& key = "",
+                     bool drop_on_hwm = true);
 
     /*!
      * \brief Return a std::string of ZMQ_LAST_ENDPOINT from the underlying ZMQ socket.
diff --git a/gr-zeromq/lib/pub_sink_impl.cc b/gr-zeromq/lib/pub_sink_impl.cc
index 460484bc0..62513000c 100644
--- a/gr-zeromq/lib/pub_sink_impl.cc
+++ b/gr-zeromq/lib/pub_sink_impl.cc
@@ -25,10 +25,11 @@ pub_sink::sptr pub_sink::make(size_t itemsize,
                               int timeout,
                               bool pass_tags,
                               int hwm,
-                              const std::string& key)
+                              const std::string& key,
+                              bool drop_on_hwm)
 {
     return gnuradio::make_block_sptr<pub_sink_impl>(
-        itemsize, vlen, address, timeout, pass_tags, hwm, key);
+        itemsize, vlen, address, timeout, pass_tags, hwm, key, drop_on_hwm);
 }
 
 pub_sink_impl::pub_sink_impl(size_t itemsize,
@@ -37,13 +38,20 @@ pub_sink_impl::pub_sink_impl(size_t itemsize,
                              int timeout,
                              bool pass_tags,
                              int hwm,
-                             const std::string& key)
+                             const std::string& key,
+                             bool drop_on_hwm)
     : gr::sync_block("pub_sink",
                      gr::io_signature::make(1, 1, itemsize * vlen),
                      gr::io_signature::make(0, 0, 0)),
       base_sink_impl(ZMQ_PUB, itemsize, vlen, address, timeout, pass_tags, hwm, key)
 {
-    /* All is delegated */
+    /* Socket option to prevent dropping of samples (backpressure) */
+    int no_drop = (drop_on_hwm == true) ? 0 : 1;
+#if USE_NEW_CPPZMQ_SET_GET
+    d_socket.set(zmq::sockopt::xpub_nodrop, no_drop);
+#else
+    d_socket.setsockopt(ZMQ_XPUB_NODROP, &no_drop, sizeof(no_drop));
+#endif
 }
 
 int pub_sink_impl::work(int noutput_items,
diff --git a/gr-zeromq/lib/pub_sink_impl.h b/gr-zeromq/lib/pub_sink_impl.h
index c97f6c40b..cedf4497a 100644
--- a/gr-zeromq/lib/pub_sink_impl.h
+++ b/gr-zeromq/lib/pub_sink_impl.h
@@ -27,7 +27,8 @@ public:
                   int timeout,
                   bool pass_tags,
                   int hwm,
-                  const std::string& key);
+                  const std::string& key,
+                  bool drop_on_hwm);
 
     int work(int noutput_items,
              gr_vector_const_void_star& input_items,
diff --git a/gr-zeromq/python/zeromq/bindings/pub_sink_python.cc b/gr-zeromq/python/zeromq/bindings/pub_sink_python.cc
index ddb3af5b3..a21b1da55 100644
--- a/gr-zeromq/python/zeromq/bindings/pub_sink_python.cc
+++ b/gr-zeromq/python/zeromq/bindings/pub_sink_python.cc
@@ -14,7 +14,7 @@
 /* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
 /* BINDTOOL_USE_PYGCCXML(0)                                                        */
 /* BINDTOOL_HEADER_FILE(pub_sink.h)                                        */
-/* BINDTOOL_HEADER_FILE_HASH(f18c40db0abeb2a88831e0f804180feb)                     */
+/* BINDTOOL_HEADER_FILE_HASH(75da3fece8cba14e97b100b327b3088f)                     */
 /***********************************************************************************/
 
 #include <pybind11/complex.h>
@@ -47,6 +47,7 @@ void bind_pub_sink(py::module& m)
              py::arg("pass_tags") = false,
              py::arg("hwm") = -1,
              py::arg("key") = "",
+             py::arg("drop_on_hwm") = true,
              D(pub_sink, make))
 
 
-- 
2.35.1

